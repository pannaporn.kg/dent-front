const routes = (module.exports = require("next-routes")());
routes
  .add("home", "/", "/home")
  .add("login", "/login")
  .add("signup", "/signup")
  .add("chat", "/chat")
  .add("present","/present/:id","present")
