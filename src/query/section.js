export default {
  defaults: [
    `id`,
    `name`,
  ],
  default: [
    `id`,
    `name`,
    `questions{
      id
      question
      answers{
        id
        name
        next
      }
    }`
  ],
  definition: [
    `id`,
    `name`,
    `def`,
    `imageId`
  ],
  history: [
    `user{
      username
      
    }`,
    `definition{
      id
      name
      def
      imageId
    }`
  ]
};
