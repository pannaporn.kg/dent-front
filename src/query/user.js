export default {
  defaults: [
    `id`,
    `titleTH`,
    `firstnameTH`,
    `lastnameTH`,
    `email`,
    `username`,
    `gecos`,
    `sysUserRoles{id sysRole{id name}}`,
    `courseOfferInstructors{
      courseOfferRole{
        id
        name
      }
    }`
  ],
  role: [`id`, `name`],
  instructorRoles: [`id`, `name`],
  instructor: [
    `id`,
    `code`,
    `sysUser{id titleTH firstnameTH lastnameTH email username gecos sysUserRoles{id sysRole{id name}}}`,
    `instructorRoleBridges{instructorRole{id name}}`
  ]
};
