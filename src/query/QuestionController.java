package com.deverhood.microservices.form.document.question;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class QuestionController implements GraphQLQueryResolver, GraphQLMutationResolver {

	@Autowired
	private QuestionService service;

	public List<Question> questions(Integer size, Integer page, String sortBy, String sort) {
		return service.findAll(size, page, sortBy, sort);
	}

	public Question question(ObjectId id) {
		return service.findById(id);
	}

	public Question insertQuestion(Question data) {
		return service.create(data);
	}

	public Question updateQuestion(ObjectId id, Question data) {
		return service.update(id, data);
	}

	public void deleteQuestion(ObjectId id) {
		service.deleteById(id);
	}
}
