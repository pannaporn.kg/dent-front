export default {
  defaults: [`id`, `code`, `name`,`refYear`, `courseKPIs {kpi{id}}`],
  default: [
    `id`,
    `code`,
    `name`,
    `refYear`,
    `courseOffers {id academicYear semesterCode year}`,
    `courseKPIs {id kpi{code question}}`
  ],
  offers: [
    `id`,
    `academicYear`,
    `semesterCode`,
    `year`,
    `course{id code name}`,
    `courseOfferInstructors{id}`,
    `answerEvidenceNotes{id}`
  ],
  offer: [
    `id`,
    `academicYear`,
    `semesterCode`,
    `year`,
    `course{code name refYear courseKPIs {id kpi{id code question meta}}}`,
    `courseOfferInstructors{id sysUser{id firstnameTH lastnameTH titleTH username} courseOfferRole{
        id
        name
      } }`,
    `answerEvidenceNotes {
      id
      sysUser {
        id
        firstnameTH
        lastnameTH
      }
      status
      kpiAnswerEvidences {
        id
        sysUser {
          firstnameTH
          lastnameTH
          id
        }
      }
      courseOfferKPIAnswers{
        id
        kpi{
          id
          code
          question
        }
      }
    }`
  ],
  form: [`id`,`courseOfferId`,`courseOfferKPIAnswers{id kpi{id code question answerTemplate}}`],
  evidence: [`id`],
  answers: [
    `id`,
    `note`,
    `courseOfferId`,
    `sysUser{id firstnameTH lastnameTH}`,
    `status`,
    `courseOfferKPIAnswers{kpi{id code question}}`,
    `kpiAnswerEvidences{id evidence{
        id
        filename
        path
        type
      } sysUser{firstnameTH lastnameTH id}}`
  ],
  instructors: [
    `id`,
    `sysUser {
      id
      firstnameTH
      lastnameTH
      email
      username
      gecos
      titleTH
    }`,
    `courseOfferRole {
      id
      name
    }`
  ]
};
