export default {
  defaults: [
    `id`,
    `code`,
    `question`,
    `meta`,
    `description`
  ],
  default:[
    `id`,
    `code`,
    `question`,
    `answerTemplate`,
    `description`,
    `meta`
  ],
  courseKPI:[
    `id`,
    `code`
  ]
};
