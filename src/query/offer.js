export default {
  defaults: [
    `id`,
    `academicYear`,
    `semesterCode`
  ]
};
