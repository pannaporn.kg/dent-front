export default{
  defaults:[
    `id`,
    `name`,
    `width`,
    `type`,
    `bySysUserId`
  ],
  node:[
    `id`,
    `name`,
    `type`,
    `fromTermRelation{
      significant
    }`,
  ]
}
