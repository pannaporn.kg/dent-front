export default {
  contains: value => {
    return { name: { $regex: value, $options: "i" } };
  },
  searchInstructor: value => {
    return {
      $or: [
        { firstnameTH: { $regex: value, $options: "i" } },
        { lastnameTH: { $regex: value, $options: "i" } }
      ]
    };
  },
  getId: id => {
    return {
      $$ROOT: {
        _id: { $oid: id }
      }
    };
  }
};
