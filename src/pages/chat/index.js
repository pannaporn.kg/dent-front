import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import withRedux from "next-redux-wrapper";
import actions from "actions";
import Header from "components/header";
import { Form, Row, Col, Card, Icon, Button, List, Avatar } from "antd";
import { SELF, GET, DEFINITION, HISTORY ,POST} from "actions/actionType";
import Main from "../main.js";
import styles from "./index.scss";
import pic from "images/pic/doc.png";
const FormItem = Form.Item;
const mapStateToProps = state => {
  return {
    common: { ...state.common },
    chat: { ...state.chat },
    section: { ...state.section }
  };
};
import query from "query";
import roles from "roles";

const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query)),
  onWatchSection: (doc, api, query, item, id) =>
    dispatch(actions.section.onWatchSection(doc, api, query, item, id)),
  onSelectAnswer: answerId => dispatch(actions.section.onSelectAnswer(answerId))
});

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      test: "nont",
      messageList: {}
    };
  }
  componentDidMount() {
    this.props.onWatchSection(
      SELF,
      GET,
      query.section.default,
      null,
      this.props.url.query.id
    );
  }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  //Logic
  scrollToBottom = () => {
    const { messageList } = this.state;
    if (messageList){
      const scrollHeight = messageList.current.scrollHeight;
      const height = messageList.current.clientHeight;
      const maxScrollTop = scrollHeight - height;
      messageList.current.scrollTop = maxScrollTop
    }
  };
//Action
  onClickHome = () => {
    this.props.redirectTo('/home')
  }
  onClickAnswer = v => {
    console.log(v);
    if (v.next) {
      this.props.onSelectAnswer(v);
    } else {
      this.props.onWatchSection(HISTORY,POST,query.section.history,{definitionName:v.name,username:this.props.common.me.username})
      this.props.onWatchSection(DEFINITION, GET, query.section.definition, {
        name: v.name
      });
    }
  };
  //Render
  render() {
    return (
      <Main>
        <style>{styles}</style>
        <Card
          style={{
            width: "350px",
            height: "548px",
            margin: "auto",
            display: "block",
            textAlign: "center",
            backgroundColor: "#f2f2f2",
            paddingTop: "15px",
            borderRadius: "8px"
          }}
        >
          <h4 style={{ fontSize: "16px", color: "#666666" }}>
            <b>
              {this.props.section.data ? this.props.section.data.name : null}
            </b>
          </h4>
          <div style={{ width: "100%" }}>
              <Card style={{ height: "450px",padding: "8px",margin: "auto",
              display: "block", }}>
              <div
                ref={this.state.messageList}
                style={{
                  backgroundColor:'white',
                  textAlign: "center",
                  overflow: "scroll",
                  height: "440px",
                }}
              >
                <List
                  dataSource={
                    this.props.section.data.chat
                      ? this.props.section.data.chat
                      : []
                  }
                  renderItem={v => (
                    <Row style={{ margin: "10px" }}>
                      {v.def ? (
                        <div>
                          {v.def}

                          {v.imageId ? <img
                            src={`${process.env.BACKEND_URL}/file/files/image/${
                              v.imageId
                            }`}
                            style={{ width: '100%' }}
                          /> : null}


                        </div>
                      ) : (
                        <div>
                          <Avatar
                            src={ !v.isMe ? pic : null}
                            size="large"
                            icon="user"
                            style={{ float: v.isMe ? "right" : "left" }}
                          />
                          <p
                            style={{
                              float: v.isMe ? "left" : "right",
                              textAlign: "left",
                              backgroundColor: v.isMe ? "#c7eafc" : "#ffe6cb",
                              color: v.isMe ? "#45829b" : "#c48843",
                              borderRadius: "6px",
                              width: "260px",
                              padding: "8px",
                              borderRightColor: "#ffe6cb",
                              fontSize: "14px"
                            }}
                          >
                            {!v.question && !v.def
                              ? "Possible diagnosis included :"
                              : null}
                            {v.question ? <b>{v.question}</b> : v.def}
                            {v.answers
                              ? v.answers.map((y, i) => (
                                  <div>
                                    ({i + 1}) {y.name}
                                  </div>
                                ))
                              : null}
                          </p>
                        </div>
                      )}
                    </Row>
                  )}
                />
                </div>
              </Card>


            <Card
              style={{
                margin: "auto",
                display: "block",
                textAlign: "center",
                backgroundColor: "#f2f2f2",
                border: "None",
                padding: "0px",
                borderRadius: "8px"
              }}
            >

              <div>
                {this.props.section.data.chat
                  ? this.props.section.data.chat.length > 0
                    ? this.props.section.data.chat[
                        this.props.section.data.chat.length - 1
                      ].answers
                      ? this.props.section.data.chat[
                          this.props.section.data.chat.length - 1
                        ].answers.map((v, i) => (
                          <Button
                            onClick={() => this.onClickAnswer(v)}
                            style={{
                              width: `${100 /
                                this.props.section.data.chat[
                                  this.props.section.data.chat.length - 1
                                ].answers.length}%`,
                              height: "42px",
                              borderRadius: "4px",
                              fontSize: "16px",
                              borderColor: "#f2f2f2",
                              backgroundColor: "#c7eafc",
                              color: "rgba(0,0,0,0.4)",
                              borderWidth: "3px",
                              padding: "2px"
                            }}
                          >
                            <b>{parseInt(i + 1)}</b>
                          </Button>
                        ))
                      : <Button
                        onClick={() => this.onClickHome()}
                        style={{
                          width: "100%",
                          height: "42px",
                          borderRadius: "4px",
                          fontSize: "16px",
                          borderColor: "#f2f2f2",
                          backgroundColor: "#c7eafc",
                          color: "rgba(0,0,0,0.4)",
                          borderWidth: "3px",
                          padding: "2px"
                        }}
                      >
                        <b> Home </b>
                      </Button>
                    : null
                  : null}
              </div>


            </Card>
          </div>
        </Card>
      </Main>
    );
  }
}
export default withRedux(store, mapStateToProps, mapDispatchToProps)(
  Form.create()(Chat)
);
