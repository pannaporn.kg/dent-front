import { Button, Row, Form, Input, Alert, Col, Icon, Card } from "antd";
import { Router } from "../../routes.js";
import actions from "actions";
import styles from "./login.scss";
import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import { connect } from "react-redux";
import stylesheet from "../../public/styles/index.less";
import imgHome from "images/logo.jpg";
import Head from "next/head";
import App from "../app";
const FormItem = Form.Item;
import { SELF, POST } from "actions/actionType";
const mapStateToProps = state => ({
  common: { ...state.common },
  auth: { ...state.auth }
});

const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query)),
  login: (username, password) =>
    dispatch(actions.auth.login(username, password)),
  onWatchLogin: (doc, api, query, item, id) =>
    dispatch(actions.login.onWatchLogin(doc, api, query, item, id))
});

class Login extends React.Component {
  constructor(props) {
    super(props);
  }
  static async getInitialProps({ query }) {
    return { query };
  }
  componentDidMount() {
    if (this.props.query.ticket) {
      this.props.onWatchLogin(SELF, POST, null, {
        ticket: this.props.query.ticket
      });
    }

  }
  componentWillMount() {}
  //Action
  _onClickSignUp = () => {
    this.props.redirectTo("/signup");
  };
  onLoginChula = () => {
    // window.location.href =
      // "https://account.it.chula.ac.th/login?service=http://localhost:3000/login";
      window.location.href =
        `https://account.it.chula.ac.th/login?service=${process.env.URL}/login`;
  };
  handleLogin = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.login(values.username, values.password);
      }
    });
  };

  render() {
    return (
      <App>
        <style> {styles}</style>
        <div
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: "100%",
            maxWidth: "300px",
            textAlign: "center"
          }}
        >
          <Card>
            <div
              style={{
                backgroundColor: "#ffe7ba",
                padding: "20px",
                textAlign: "center"
              }}
            >
              <img
                style={{
                  width: "100px",
                  height: "100px",
                  objectFit: "contain",
                  marginBottom: "8px",
                  borderRadius: "40px"
                }}
                src={imgHome}
              />
              <div
                style={{
                  fontSize: "18px",
                  textAlign: "center",
                  marginBottom: "16px"
                }}
              >
                <b>{process.env.APP_NAME}</b>
              </div>
              {(this.props.common.me !== undefined
                ? this.props.auth.error
                : false) && !this.props.common.isLoading ? (
                <Alert
                  type="error"
                  message={
                    this.props.auth !== undefined ? this.props.auth.error : null
                  }
                  style={{ marginBottom: "15px" }}
                  showIcon
                />
              ) : null}
              <form onSubmit={this.handleLogin} style={{ textAlign: "center" }}>
                <Row>

                  <Button
                    onClick={() => this.onLoginChula()}
                    style={{ maxWidth: "250px", width: "100%", backgroundColor: "#85a5ff", color: "white" }}
                  >
                    Login using chula account
                  </Button>
                </Row>
              </form>
            </div>
          </Card>
        </div>
      </App>
    );
  }
}

const WrappedLoginForm = Form.create()(Login);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrappedLoginForm);
