import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import withRedux from "next-redux-wrapper";
import actions from "actions";
import Header from "components/header";
import {SELF, GET, ALL, STAT} from 'actions/actionType'
import { Form, Row, Col, Card, Icon, Button,Spin } from "antd";
import {Bar} from "react-chartjs-2"
import Main from "../main.js";
import query from 'query'
const FormItem = Form.Item;
const mapStateToProps = state => {
  return {
    common: { ...state.common },
    section: { ...state.section },
    history:{...state.history}
  };
};
import roles from "roles";
const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query)),
  onWatchHistory: (doc, api, query, item, id) =>
    dispatch(actions.history.onWatchHistory(doc, api, query, item, id))
});

class Stat extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.onWatchHistory(STAT, GET)
  }
  //Logic
  //Action
  //Render
  renderGraph() {
    const data = {
      labels:
        this.props.history
          ? this.props.history.list.map(
              v => v.definitionName
            )
          : [],
      datasets: [
        {
          label: "data",
          data:
            this.props.history
              ? this.props.history.list.map(
                  v =>
                    v.count
                )
              : [],
          backgroundColor: (this.props.history ? this.props.history.list : []).map(
            v =>
              v.approve > v.minRequire
                ? "rgba(82, 196, 26, 0.2)"
                : "rgba(150, 150, 150, 0.2)"
          ),
          borderColor: (this.props.history ? this.props.history.list : []).map(
            v =>
              v.approve > v.minRequire
                ? "rgba(82, 196, 26, 1)"
                : "rgba(150, 150, 150, 1)"
          ),
          borderWidth: 1
        }
      ]
    };
    const options = {
      scales: {
        xAxes: [
          {
            ticks: {
              autoSkip: false
            },
            barPercentage: 0.4
          }
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      },
      legend: {
        position: "top"
      }
    };
    return (
        <Bar height={600} width={1000} data={data} options={options} />
    );
  }

  render() {
    return (
        <Main>
        <Card style={{overflow:'auto'}}>
          <h3><b>Stat Report</b></h3>
          {this.renderGraph()}
          </Card>
      </Main>
    );
  }
}
export default withRedux(store, mapStateToProps, mapDispatchToProps)(
  Form.create()(Stat)
);
