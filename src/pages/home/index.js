import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import { connect } from "react-redux";
import actions from "actions";
import Header from "components/header";
import {SELF,ALL} from 'actions/actionType'
import { Form, Row, Col, Card, Icon, Button } from "antd";
import Main from "../main.js";
import query from 'query'
const FormItem = Form.Item;
const mapStateToProps = state => {
  return {
    common: { ...state.common },
    section: { ...state.section }
  };
};
import roles from "roles";
const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query)),
  onWatchSection: (doc, api, query, item, id) =>
    dispatch(actions.section.onWatchSection(doc, api, query, item, id))
});

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      test: "nont"
    };
  }
  componentDidMount() {
    this.props.onWatchSection(SELF,ALL,query.section.defaults)
  }
  //Logic
  //Action
  onClickSection = v => {
    this.props.redirectTo("/chat?id="+v.id);
  };
  //Render
  render() {
    return (
      <Main>
        <Card
          style={{
            width: "300px",
            margin: "auto",
            display: "block",
            textAlign: "center",
            borderRadius: "10px"
          }}
        >
          <h3><b>Menu</b></h3>
          <p>Please select category of the lesion</p>
          <br />
          <div>
            {this.props.section.list.map(v => (
              <Button
                id={v.id}
                onClick={() => this.onClickSection(v)}
                style={{
                  background: "#317ace",
                  color: "#ffffff",
                  height: "50px",
                  fontSize: "16px"
                }}
                block
              >
                {v.name}
              </Button>
            ))}
          </div>
        </Card>
      </Main>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
