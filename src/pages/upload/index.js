import { Button, Row, Form, Input, Alert, Col, Icon, Card, Upload } from "antd";
const { Dragger } = Upload;
import { Router } from "../../routes.js";
import actions from "actions";
import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import { connect } from "react-redux";
import stylesheet from "../../public/styles/index.less";
import imgHome from "images/logo.jpg";
import Head from "next/head";
import App from "../app";
const FormItem = Form.Item;
import { SELF, POST } from "actions/actionType";
const mapStateToProps = state => ({
  common: { ...state.common },
  auth: { ...state.auth }
});

const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query)),
  login: (username, password) =>
    dispatch(actions.auth.login(username, password)),
  onWatchLogin: (doc, api, query, item, id) =>
    dispatch(actions.login.onWatchLogin(doc, api, query, item, id))
});

class UploadFile extends React.Component {
  constructor(props) {
    super(props);
  }
  static async getInitialProps({ query }) {
    return { query };
  }
  componentDidMount() {
    if (this.props.query.ticket) {
      this.props.onWatchLogin(SELF, POST, null, {
        ticket: this.props.query.ticket
      });
    }
  }
  componentWillMount() {}
  //Action
  _onClickSignUp = () => {
    this.props.redirectTo("/signup");
  };
  onLoginChula = () => {
    // window.location.href =
    // "https://account.it.chula.ac.th/login?service=http://localhost:3000/login";
    window.location.href = `https://account.it.chula.ac.th/login?service=${process.env.URL}/login`;
  };
  handleLogin = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.login(values.username, values.password);
      }
    });
  };

  render() {
    const props = {
      name: "file",
      multiple: true,
      action: `/file/files/`,
      onChange(info) {
        const { status } = info.file;
        if (status !== "uploading") {
          console.log(info.file, info.fileList);
        }
        if (status === "done") {
          message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === "error") {
          message.error(`${info.file.name} file upload failed.`);
        }
      }
    };
    return (
      <App>
        <Card>
          <div> UPLOAD </div>
          <Dragger {...props}>
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">
              Click or drag file to this area to upload
            </p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from
              uploading company data or other band files
            </p>
          </Dragger>
        </Card>
      </App>
    );
  }
}

const WrappedLoginForm = Form.create()(UploadFile);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrappedLoginForm);
