import React from "react";
import ReactDOM from "react-dom";
import store from "store";
import withRedux from "next-redux-wrapper";
import { Action } from "deverhood-react";
import Header from "components/header";
import { Form, Row, Col, Card, Icon, Button, List, Avatar } from "antd";
import Main from "../main.js";
import styles from './index.scss'
const FormItem = Form.Item;
const mapStateToProps = state => {
  return {
    common: { ...state.common },
    definition:{...state.definition}
  };
};
import query from "query";
import roles from "roles";
const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(Action.common.redirectTo(redirectTo, query))
});

class Definition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      test:"nont"
    };
  }
  componentDidMount() {}
  //Logic
  //Action
  //Render
  render() {
    return (
      <Main>
      <style>{styles}</style>

      <Card style={{width:'330px',height:'560px',margin:'auto',display:'block',textAlign:'center',backgroundColor:'#f2f2f2',borderRadius:"10px",paddingTop: "15px"}}>
      <h4 style={{fontSize:"16px",color:"#666666"}}>Definition</h4>


      <Card style={{width:'325px',height:'500px',margin:'auto',display:'block',textAlign:'center',padding:'30px',paddingTop:'10px',overflow:'scroll'}}>
      <List dataSource={this.props.definition.list}
      renderItem={v => (
        <div>
        <br></br>
        <h4>{v.disease}</h4>
        <br></br>
        <p>{v.def}</p>
        </div>
      )}/>
      </Card>
</Card>
      </Main>
    );
  }
}
export default withRedux(store, mapStateToProps, mapDispatchToProps)(
  Form.create()(Definition)
);
