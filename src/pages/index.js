import React from "react";
import Main from "./main.js";
import withRedux from "next-redux-wrapper";
import store from "store";
import { Card, Table, Icon } from "antd";
import {Action} from "deverhood-react";
const mapStateToProps = state => {
  return { ...state.auth };
};

const mapDispatchToProps = dispatch => ({
  onLogin: (username, password) =>
    dispatch(Action.auth.login(username, password))
});
class Home extends React.Component {
  //Render
  render() {
    return (
      <Main>

      </Main>
    );
  }
}

export default withRedux(store, mapStateToProps, mapDispatchToProps)(Home);
