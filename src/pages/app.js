import React from "react";
import { Router } from "../routes.js";
import { Action, ActionType, Query } from "deverhood-react";
import Head from "next/head";
import { connect } from "react-redux";
import stylesheet from "../public/styles/index.less";
import { Spin, message } from "antd";
const { GET, ME } = ActionType;
const isProd = process.env.NODE_ENV === "production";

const mapStateToProps = state => {
  return { ...state.common };
};
const mapDispatchToProps = dispatch => ({
  onRedirect: previousRoute => dispatch(Action.common.redirect(previousRoute)),
  onWatchUser: (doc, api, item, id, query) =>
    dispatch(Action.user.onWatchUser(doc, api, item, id, query)),
   me: url => dispatch(Action.auth.me(url))
});

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      Router.pushRoute(nextProps.redirectTo, nextProps.query);
      this.props.onRedirect(Router.route);
    }
  }

  componentDidMount() {
    this.props.me()
  }
  error = () => {
    message.error(`${this.props.message}`);
  };
  success = () => {
    message.success("success");
  };
  render() {
    return (
      <div>
        <Head>
          <title>Deverhood</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <style
            dangerouslySetInnerHTML={{
              __html: stylesheet
            }}
          />
        </Head>
        {this.props.isSuccess ? this.success() : null}
        {this.props.isFail ? this.error() : null}
        {this.props.children}
        {/* {this.props.appLoaded ? (
          this.props.children
        ) : (
          <Spin size="large" className="is-center" />
        )} */}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
