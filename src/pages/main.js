import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { Layout, Card } from "antd";
import { Row, Col } from "antd";
import Header from "components/layout/Header.js";
import Footer from "components/layout/Footer.js";
import App from "./app.js";
import AppSider from "components/layout/Sider.js";
const mapStateToProps = state => {
  return {
    ...state.setting
  };
};

const mapDispatchToProps = dispatch => ({});
class Main extends React.Component {
  render() {
    return (
      <div>
        <div
          style={{
            position: "fixed",
            width: "100vw",
            height: "100vh",
            backgroundImage: `url(${this.props.background})`,
            backgroundColor: "#ffc66a"
          }}
        />

        <App>
          <Layout style={{ height: "100vh" }}>
          <Header />
            <Layout style={{ height: "100%" }}>
              <Layout.Content
                style={{
                  width: "100%",
                  padding: "10px 10px",
                }}
              >
                <Row style={{ height: "100%" }}>
                  <Col sm={{ span: 1 }} lg={{ span: 2 }} />
                  <Col
                    xs={{ span: 24 }}
                    sm={{ span: 22 }}
                    lg={{ span: 20, offset: 0 }}
                    style={{ padding: "10px",marginTop:'64px' }}
                  >
                    {this.props.children}
                  </Col>
                  <Col sm={{ span: 1 }} lg={{ span: 2 }} />
                </Row>
              </Layout.Content>
            </Layout>
          </Layout>
        </App>
      </div>
    );
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
