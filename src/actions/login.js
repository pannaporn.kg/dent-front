import {
  LOGIN_REQUEST
} from "actions/actionType";

export default {
  onWatchLogin: (doc,api,query,item,id) => ({
    type:LOGIN_REQUEST,
    api,
    doc,
    query,
    item,
    id
  }),
};
