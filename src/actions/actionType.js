const REQUEST = "REQUEST";
const SENDING = "SENDING";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";
const CALLBACK_SUCCESS = "CALLBACK_SUCCESS";
const CALLBACK_FAIL = "CALLBACK_FAIL";
const CALLBACK_LOADING = "CALLBACK_LOADING";
const CALLBACK_OUT = "CALLBACK_OUT"
export const CHANGE_ANSWER_KPI = "CHANGE_ANSWER_KPI"
export const ON_UPLOAD_FILE = "ON_UPLOAD_FILE"
//REST APIs
export const GET = "GET";
export const POST = "POST";
export const PUT = "PUT";
export const DEL = "DEL";
export const GETID = "GETID";
export const ALL = "ALL";
export const SEARCH = "SEARCH";
export const DEFAULT = "DEFAULT";
export const STUDENT = "STUDENT";
export const TEACHER = "TEACHER";
export const STAFF = "STAFF";
export const SELF = "SELF";
export const SCHOOL = "SCHOOL";
export const OFFER = "OFFER";
export const ROLE = "ROLE"
export const INSTRUCTOR = "INSTRUCTOR"
export const INSTRUCTORROLE = "INSTRUCTORROLE"
export const EVIDENCE = "EVIDENCE"
export const ANSWER = "ANSWER"
export const FILE = "FILE"
export const DEFINITION = "DEFINITION"
export const HISTORY = "HISTORY"
export const STAT = "STAT"
const groupKPI = [DEFAULT];
const groupCourse = [DEFAULT, OFFER,INSTRUCTOR,EVIDENCE,ANSWER,FILE];
const groupUser = [DEFAULT,ROLE,INSTRUCTOR,INSTRUCTORROLE];
const groupSection = [SELF,DEFINITION,HISTORY]
const groupHistory = [SELF, DEFINITION, HISTORY, STAT]
//$(document).ready(function() {
//});
function createDocumentRequestTypes(base) {
  return groupDocument.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createKPI(base) {
  return groupKPI.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createLogin(base) {
  return ["SELF","LOGOUT"].reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createCourse(base) {
  return groupCourse.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createUser(base) {
  return groupUser.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createSection(base) {
  return groupSection.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createHistory(base) {
  return groupHistory.reduce((acc, type) => {
    acc[type] = createAPIRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}
function createAPIRequestTypes(base) {
  return [ALL, GETID, GET, POST, PUT, DEL, SEARCH].reduce((acc, type) => {
    acc[type] = createRequestTypes(`${base}_${type}`);
    return acc;
  }, {});
}

function createRequestTypes(base) {
  return [
    REQUEST,
    SENDING,
    SUCCESS,
    FAILURE,
    CALLBACK_SUCCESS,
    CALLBACK_FAIL,
    CALLBACK_LOADING,
    CALLBACK_OUT
  ].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}
export const STUDENT_PROGRAM_CHANGE = "STUDENT_PROGRAM_CHANGE";
export const LOGIN = createRequestTypes("LOGIN");
export const ME = createRequestTypes("ME");
export const LOGOUT = "LOGOUT"
export const COMPONENT = createRequestTypes("COMPONENT");
export const USER = createRequestTypes("USER");
export const APP_LOAD = "APP_LOAD";
export const REDIRECT_TO = "REDIRECT_TO";
export const REDIRECT = "REDIRECT";
export const RELOAD = "RELOAD";
export const KPI_REQUEST = "KPI_REQUEST";
export const KPI_API = createKPI("KPI_API");
export const COURSE_REQUEST = "COURSE_REQUEST";
export const COURSE_API = createCourse("COURSE_API");
export const USER_REQUEST = "USER_REQUEST";
export const USER_API = createUser("USER_API");
export const SECTION_REQUEST = "SECTION_REQUEST"
export const SECTION_API = createSection("SECTION_API")
export const SELECT_ANSWER = "SELECT_ANSWER"
export const LOGIN_API  = createLogin("LOGIN_API")
export const LOGIN_REQUEST = "LOGIN_REQUEST"
export const HISTORY_API  = createHistory("HISTORY_API")
export const HISTORY_REQUEST = "HISTORY_REQUEST"
