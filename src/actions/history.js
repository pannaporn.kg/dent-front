import {
  HISTORY_REQUEST
} from "actions/actionType";

export default {
  onWatchHistory: (doc,api,query,item,id) => ({
    type:HISTORY_REQUEST,
    api,
    doc,
    query,
    item,
    id
  }),
};
