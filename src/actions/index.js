
import auth from './auth'
import common from './common'
import section from './section'
import login from './login'
import history from './history'
export default {auth, common, section, login, history}
