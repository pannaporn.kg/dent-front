import {
  SECTION_REQUEST,SELECT_ANSWER
} from "actions/actionType";

export default {
  onWatchSection: (doc,api,query,item,id) => ({
    type:SECTION_REQUEST,
    api,
    doc,
    query,
    item,
    id
  }),
  onSelectAnswer:(answer) => ({
    type:SELECT_ANSWER,
    answer
  })
};
