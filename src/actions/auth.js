import { LOGIN, ME, LOGOUT } from "actions/actionType";

export default {
  login: (username, password) => ({
    type: LOGIN.REQUEST,
    username,
    password
  }),
  me: (username, password) => ({
    type: ME.REQUEST
  }),
  logout: () => ({
    type: LOGOUT.REQUEST
  })
};
