import { REDIRECT_TO, REDIRECT } from "actions/actionType";
export default {
  redirect: previousRoute => ({
    type: REDIRECT,
    previousRoute: previousRoute
  }),
  redirectTo: (redirectTo, query) => ({
    type: REDIRECT_TO,
    redirectTo: redirectTo,
    query: query
  })
};
