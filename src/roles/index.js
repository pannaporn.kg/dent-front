
export default {
  menus(roles) {
    return menu.filter(v => this.readPermission(roles, v.id));
  },
  advisors(roles, users, id) {
    return advisors.filter(v => this.readPermission(roles, v.id)).map(
      v =>
        v.id === "7"
          ? {
              ...v,
              users: users.filter(
                i =>
                  i.roles.filter(j => j.name === "TEACHER").length > 0 &&
                  i.id !== id
              )
            }
          : v.id === "8"
            ? {
                ...v,
                users: users.filter(
                  i =>
                    i.roles.filter(j => j.name === "STUDENT").length > 0 &&
                    i.id !== id
                )
              }
            : null
    );
  },
  logbook(roles){
    return logbookAction.filter(v => this.readPermission(roles,v.id))
  },
  checkMe(mid,fid){
    return mid === fid
  },
  readPermission(roles, id) {
    var permission = [];
    var found = false
    for (var i in roles) {
        for (var j in perms){
          found = roles[i] === perms[j]
          if (found){ return found }
        }
    }
    return found
  },
};
