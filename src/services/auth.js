import agent from "./agent";
const url = "/wfme/graphql"
export default {
  login(username, password) {
    return agent.post(url, {
      query: `mutation{
        login(data:{username:"${username}",password:"${password}"}){
          token
        }
      }`
    });
  },
  me() {
    return agent.post(url, {
      query: `{
        me{
          id
          sysUserRoles{
            sysRole{
              name
            }
          }
          firstnameTH
          lastnameTH
          firstnameEN
          lastnameEN
          email
        }
      }`
    });
  },
  logout() {
   return agent.post(url, {
      query: `{
        logout
      }`
    });
  },
  ping() {
    return agent.get("/wfme/resources/commons/ping")
  }
};
