import agent from "./agent";
import axios from 'axios'
const convert = item => {
  return JSON.stringify(item).replace(/\"([^(\")"]+)\":/g, "$1:");
};
const url = "/dent/graphql"
export default {

  //Graphql
  get: (methods, actions) => {
    return agent.post(url, {
      query: `query {${methods}{${actions.query.map(v => v + " ")}}}`
    });
  },
  filter: (methods, actions) => {
    return agent.post(url, {
      query: `query ${methods}($filter:JsonString){${methods}(filter:$filter){${actions.query.map(v => v + " ")}}}`,
      variables: { "filter":JSON.stringify(actions.item) }
    });
  },
  post: (inputClass, methods, actions) => {
    return agent.post(url, {
      query: `mutation ${methods}($data:${inputClass}){${methods}(data:$data){${actions.query.map(
        v => v + " "
      )}}}`,
      variables:{"data":actions.item}
    });
  },
  put: (inputClass, methods, actions) => {
    return agent.post(url, {
      query: `mutation ${methods}($data:${inputClass}){${methods}(data:$data,id:"${
        actions.id
      }"){${actions.query.map(v => v + " ")}}}`,
      variables: {"data":actions.item}
    });
  },
  delete: (methods, actions) => {
    return agent.post(
      url,
      {
        query: `mutation ${methods}($id:ObjectId!){${methods}(id:$id){id}}`,
        variables:{ id: actions.id }
      }

    );
  },
  searchUser:(methods, actions) => {
    return agent.post(url, {
      query: `query ${methods}($sysUserFilter:JsonString){${methods}(sysUserFilter: $sysUserFilter){${actions.query.map(v => v + " ")}}}`,
      variables: { "sysUserFilter":JSON.stringify(actions.item) }
    });
  },
  //Service
  restPost: (url,item) => {
    return agent.post(url,item)
  },
  restGet: (url,item) => {
    return agent.get(url,item)
  }
};
