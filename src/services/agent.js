import axios from "axios";
import Cookies from "universal-cookie";
const cookies = new Cookies();
const API_ROOT = "/api";
const headers = () => ({
  "Content-Type": "application/json",
  Accept: "application/json",
});

const agent = axios.create({
  baseURL: `${API_ROOT}`,
  headers: headers(),
  timeout: 20000,
  withCredentials: true
});

export default agent;
