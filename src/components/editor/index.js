import React from "react";
import {
  convertFromHTML,
  EditorState,
  convertToRaw,
  ContentState
} from "draft-js";
import { convertToHTML } from "draft-convert";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import styles from "./editor.scss";
import debounce from "lodash.debounce";
class EditorConvertToHTML extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editor: null,
      editorState: this.getInitialStateFromHTMLValue(props.html)
    };
  }
  componentWillUnmount() {
    this.unmounted = true;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.html !== this.props.html) {
      this.setState({
        editorState: this.getUpdatedStateFromHTMLValue(nextProps.html)
      });
    }
  }
  getInitialStateFromHTMLValue = value => {
    let editorState;
    if (value) {
      const blocksFromHTML = convertFromHTML(value);
      const contentState = ContentState.createFromBlockArray(blocksFromHTML);
      editorState = EditorState.createWithContent(contentState);
      //editorState = EditorState.set(editorState);
    } else {
      editorState = EditorState.createEmpty();
    }

    return editorState;
  };
  getUpdatedStateFromHTMLValue = value => {
    const { editorState } = this.state;
    const blocksFromHTML = convertFromHTML(value);
    const nextContentState = ContentState.createFromBlockArray(blocksFromHTML);

    return EditorState.push(editorState, nextContentState);
  };
  componentDidMount() {
    this.setState({
      editor: Editor
    });
  }
  debouncedOnChange = debounce(this.props.onChange, 100);
  onEditorStateChange = editorState => {
    const prevValue = convertToHTML(this.state.editorState.getCurrentContent());
    const nextValue = convertToHTML(editorState.getCurrentContent());
    if (!this.unmounted) {
      this.setState({ editorState }, () => {
        if (prevValue !== nextValue) {
          this.debouncedOnChange(nextValue);
        }
      });
    }
  };
  getCurrentContent = () => {
    return draftToHtml(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
  };
  render() {
    const ClientEditor = this.state.editor;
    return (
      <div
        className="container"
        style={{ border: "1px solid #d9d9d9", padding: "8px" }}
      >
        <style jsx>{styles}</style>
        {this.state.editor ? (
          <ClientEditor
            editorState={this.state.editorState}
            wrapperClassName="editor-wrapper"
            editorClassName="editor-editor"
            onEditorStateChange={this.onEditorStateChange}
          />
        ) : null}
      </div>
    );
  }
}
export default EditorConvertToHTML;
