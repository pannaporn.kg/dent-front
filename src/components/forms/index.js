import React from "react";
import {
  Table,
  Row,
  Collect,
  Button,
  Modal,
  Form,
  Input,
  Popconfirm,
  Icon,
  InputNumber,
  Select,
  Popover,
  Tag,
  Col,
  Badge,
  Tabs,
  Card,
  Radio,
  DatePicker,
  Slider,
  Upload
} from "antd";
const RadioGroup = Radio.Group;
const Option = Select.Option;
const TabPane = Tabs.TabPane;
const RadioButton = Radio.Button;
import {
  INSTITUTE,
  USER,
  TEACHER,
  ROLE,
  GETID,
  GET,
  PUT,
  POST,
  DEL,
  SIGNUP
} from "actions/actionType";
const FormItem = Form.Item;
import roles from "roles";
import styles from "./index.scss";
export default class NForm extends React.Component {
  constructor(props) {
    super(props);
  }
  _onSearch = value => {};
  renderText() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(<Input />)}
        </FormItem>
      </div>
    );
  }
  renderTextArea() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <Input.TextArea
              placeholder={
                this.props.placeholder
              }
              style={{minHeight:'100px'}}
              autosize
            />
          )}
        </FormItem>
      </div>
    );
  }
  renderNumber() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <InputNumber
              parser={value => value.replace(/[^0-9]/g, "")}
              style={{ width: "300px" }}
            />
          )}
        </FormItem>
      </div>
    );
  }
  renderRadio() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <RadioGroup>
              {this.props.choices.map(v => (
                <Radio value={v.id} key={v.key}>
                  {v.value}
                </Radio>
              ))}
            </RadioGroup>
          )}
        </FormItem>
      </div>
    );
  }
  renderSelect() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <Select
              onSearch={value => this._onSearch(value)}
              showSearch
              filterOption={(input, option) =>
                option.props.children
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
            >
              {this.props.choices.map(v => (
                <Option value={v.id} key={v.key}>
                  {v.value}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
      </div>
    );
  }
  renderRadioButton() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <RadioGroup>
              {this.props.choices.map(v => (
                <RadioButton value={v.value} key={v.key}>
                  {v.value}
                </RadioButton>
              ))}
            </RadioGroup>
          )}
        </FormItem>
      </div>
    );
  }
  renderDatePicker() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? null : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(<DatePicker />)}
        </FormItem>
      </div>
    );
  }
  renderParagraph() {
    return (
      <div
        key={this.props.formKey}
        style={{
          marginBottom: "16px",
          fontSize: "16px",
          fontWeight: "300",
          color: "black"
        }}
      >
        {this.props.message}
      </div>
    );
  }
  renderDivider() {
    return (
      <div
        style={{ borderBottom: "1px solid #e8e8e8", marginBottom: "16px" }}
      />
    );
  }
  renderSlider() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? [0, 0] : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(<Slider range max={this.props.max} />)}
        </FormItem>
      </div>
    );
  }
  renderUpload() {
    return (
      <div>
        <FormItem
          key={this.props.formKey}
          label={this.props.message}
          {...this.props.formItemLayout}
        >
          {this.props.getFieldDecorator(`${this.props.formKey}`, {
            initialValue:
              this.props.value === undefined ? [0, 0] : this.props.value,
            rules: [
              {
                required: this.props.required,
                message: `ต้องกรอก ${this.props.message}`
              }
            ]
          })(
            <Upload name="logo" action="/upload.do" listType="picture">
              <Button>
                <Icon type="upload" /> Click to upload
              </Button>
            </Upload>
          )}
        </FormItem>
      </div>
    );
  }
  render() {
    return (
      <div className="containers">
        <style> {styles} </style>
        {this.props.type === "text"
          ? this.renderText()
          : this.props.type === "number"
            ? this.renderNumber()
            : this.props.type === "radio"
              ? this.renderRadio()
              : this.props.type === "radioButton"
                ? this.renderRadioButton()
                : this.props.type === "datePicker"
                  ? this.renderDatePicker()
                  : this.props.type === "paragraph"
                    ? this.renderParagraph()
                    : this.props.type === "select"
                      ? this.renderSelect()
                      : this.props.type === "divider"
                        ? this.renderDivider()
                        : this.props.type === "textArea"
                          ? this.renderTextArea()
                          : this.props.type === "slider"
                            ? this.renderSlider()
                            : this.props.type === "upload"
                              ? this.renderUpload()
                              : null}
      </div>
    );
  }
}
