import React from "react";
import Head from "next/head";
import Router from "next/router";
import header from "./header.scss";
import {Action} from "deverhood-react";
import actions from 'actions'
import {LOGOUT,POST} from 'actions/actionType'
import { connect } from "react-redux";
import { Layout, Menu, Icon, Row, Input } from "antd";
const Search = Input.Search;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
import roles from "roles";
const mapStateToProps = state => {
  return { ...state.common };
};

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(Action.auth.logout()),
  redirectTo: (path, query) => dispatch(Action.common.redirectTo(path, query)),
});

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      current: "mail"
    };
  }
  _onLogout = () => {
    this.props.onLogout()
    window.location.href =
      "https://account.it.chula.ac.th/logout?service=http://localhost:3000/login"
  };
  handleClickMenu = e => {
    switch (e.key) {
      case "logout":
        return this._onLogout()
      default:
        this.props.redirectTo(e.key, {});
    }
  };

  render() {
    return (
      <Row
        style={{
          position: "fixed",
          zIndex: 1,
          width: "100%"
        }}
        className="header"
      >
        <style> {header} </style>
        <Menu
          style={{
            padding: "0 0",
            textAlign: "right"
          }}
          selectedKeys={[this.state.current]}
          mode="horizontal"
          onClick={this.handleClickMenu}
        >
          <Menu.Item key="/home">
            <Icon type="home" />
          </Menu.Item>
          <Menu.Item key="/stat">
            <Icon type="bar-chart" />
          </Menu.Item>
          <SubMenu
            title={
              <span className="submenu-title-wrapper">
                <Icon
                  style={{ fontSize: "18px" }}
                  type="ellipsis"
                  theme="outlined"
                />
              </span>
            }
          >
            <Menu.Item key="logout">
              <Icon type="logout" theme="outlined" />
              Logout
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Row>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
