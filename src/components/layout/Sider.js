import React from "react";
import Head from "next/head";
import Router from "next/router";
import header from "./header.scss";
import {Action} from "deverhood-react";
import { connect } from "react-redux";
import { Layout, Menu, Icon } from "antd";
import roles from "roles";
const mapStateToProps = state => {
  return { ...state.common };
};

const mapDispatchToProps = dispatch => ({
  redirectTo: (path, query) => dispatch(Action.common.redirectTo(path, query)),
  logout: () => dispatch(Action.auth.logout())
});

class AppSider extends React.Component {
  constructor() {
    super();
  }
  logout() {
    this.props.logout();
  }
  handleClickMenu = e => {
    switch (e.key) {
      case "logout":
        this.props.onLogout();
        break;
      default:
        this.props.redirectTo(e.key, {});
    }
  };

  render() {
    return (
      <Layout.Sider
        style={{ background: "#eeeeee", zIndex: "1", marginTop: "0px" }}
        breakpoint="lg"
        collapsedWidth="0"
        onCollapse={(collapsed, type) => {}}
      >
        <Menu
          mode="inline"
          style={{ height: "100%", border: "1px solid #eeeeee" }}
          defaultSelectedKeys={["1"]}
          onClick={this.handleClickMenu}
        >
          <Menu.Item key="/">
            <span>
              <Icon type="home" />
              Home
            </span>
          </Menu.Item>
          <Menu.Item key="/user">
            <span>
              <Icon type="team" />
              Team
            </span>
          </Menu.Item>
          <Menu.Item key="/project">
            <span>
              <Icon type="tablet" />
              Project
            </span>
          </Menu.Item>

          <Menu.Item key="logout">
            <span>
              <Icon type="home" />
              Logout
            </span>
          </Menu.Item>
        </Menu>
      </Layout.Sider>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppSider);
