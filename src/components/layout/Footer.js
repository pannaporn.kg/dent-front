import React from 'react';
import {Layout} from 'antd';

class Footer extends React.Component {
    render() {
        return (
            <Layout.Footer style={{
                textAlign: 'center',
                background:'#eeeeee'
            }}>
                Medic ©2018 Developed by Deverhood Co.,Ltd.
            </Layout.Footer>
        );
    }
}

export default Footer
