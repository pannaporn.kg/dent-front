import React from "react";
import store from "store";
import withRedux from "next-redux-wrapper";
const mapStateToProps = state => {
  return {
    common: { ...state.common }
  };
};
const mapDispatchToProps = dispatch => ({
  redirectTo: (redirectTo, query) =>
    dispatch(actions.common.redirectTo(redirectTo, query))
});
import InfiniteScroll from "react-infinite-scroller";
import { Row, Col, Card, Icon, List, Skeleton } from "antd";
import styles from "./index.scss";
class InfList extends React.Component {
  render() {
    return (
      <div className="line">
        <style>{styles}</style>
        <InfiniteScroll
          initialLoad={false}
          pageStart={0}
          loadMore={this.props._onLoadMore()}
          hasMore={
            !this.props.common.isLoading && !this.props.common.isOutOfRange
          }
          useWindow={true}
        >
          <List

            dataSource={this.props.dataSource}
            renderItem={item => (
              <List.Item onClick={() => this.props._onClickItem(item)} key={item.id}>{this.props.renderItem(item)}</List.Item>
            )}
          >
            {this.props.common.isLoading ? (
              <Skeleton style={{ padding: "16px" }} active avatar>
                <List.Item.Meta avatar="" title="" description="" />
              </Skeleton>
            ) : null}
          </List>
        </InfiniteScroll>
      </div>
    );
  }
}
export default withRedux(store, mapStateToProps, mapDispatchToProps)(InfList);
