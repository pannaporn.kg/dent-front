import React from "react";
import { Card, Icon } from "antd";
import store from "store";
import withRedux from "next-redux-wrapper";
const mapStateToProps = state => {};
const mapDispatchToProps = dispatch => ({});
class Header extends React.Component {
  render() {
    return (
      <h4>
        <Icon type={this.props.icon} style={{ marginRight: "8px" }} />
        {this.props.header}
      </h4>
    );
  }
}
export default withRedux(store, mapStateToProps, mapDispatchToProps)(Header);
