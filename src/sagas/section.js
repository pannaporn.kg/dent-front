import {
  SECTION_API,
  SECTION_REQUEST,
  GET,
  POST,
  PUT,
  DEL,
  ALL,
  SELF,
  COMPONENT,
  DEFINITION,
  HISTORY
} from "actions/actionType.js";
import { put, fork, call, take, takeEvery } from "redux-saga/effects";
import graphql from "services/graphql";
import query from "query";
function* callback(isSuccess, error) {
  isSuccess
    ? yield put({
        type: COMPONENT.CALLBACK_SUCCESS
      })
    : yield put({
        type: COMPONENT.CALLBACK_FAIL,
        data: error.response.data.message
      });
  yield put({
    type: COMPONENT.SUCCESS
  });
}
function* loading() {
  return yield put({
    type: COMPONENT.CALLBACK_LOADING
  });
}
function* complete() {
  return yield put({
    type: COMPONENT.SUCCESS
  });
}
function* outOfRange() {
  yield put({
    type: COMPONENT.CALLBACK_OUT
  });
}
function* request(actions) {
  switch (actions.api) {
    case GET:
      return yield fork(get, actions);
    case POST:
      return yield fork(post, actions);
    case PUT:
      return yield fork(change, actions);
    case DEL:
      return yield fork(del, actions);
    case ALL:
      return yield fork(getAll, actions);
    case SEARCH:
      return yield fork(search, actions);
    default:
      null;
  }
}
function* getAll(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {
      case SELF:
        var response = yield call(graphql.get, "sections", actions);
        yield put({
          type: SECTION_API.SELF.ALL.SUCCESS,
          data: response.data.data.sections
        });
        return yield call(complete);
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* get(actions) {
  try {
    switch (actions.doc) {
      case SELF:
        var response = yield call(graphql.get, `section(id:"${actions.id}")`,actions);
        return yield put({
          type: SECTION_API.SELF.GET.SUCCESS,
          data: response.data.data.section
        });
        case DEFINITION:
        var response = yield call(graphql.get, `definition(name:"${actions.item.name}")`,actions);
        return yield put({
          type: SECTION_API.DEFINITION.GET.SUCCESS,
          data: response.data.data.definition
        });
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* post(actions) {
  try {
    switch (actions.doc) {

//////////////////////////////////
      case HISTORY:
      var response = yield call(graphql.post,'HistoryInput','insertHistory', actions);
      yield put({
        type: SECTION_API.HISTORY.POST.SUCCESS,
        data: response.data.data.insertHistory
      });
      return yield call(callback, true);
//////////////////////////////////

      default:
        null;
    }
  } catch (e) {
    console.log(e);
    yield call(callback, false, e);
  }
}
function* change(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    yield call(callback, false, e);
  }
}
function* del(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* search(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
export function* watchSection() {
  yield takeEvery(SECTION_REQUEST, request);
}
