import { all, fork } from "redux-saga/effects";
import {watchSection} from './section'
import {watchLogin} from './login'
import {watchHistory} from './history'
import {Saga} from 'deverhood-react'

export default function* rootSaga() {
  yield all([
    watchSection(),
    watchLogin(),
    watchHistory(),
    Saga.watchUser(),
    Saga.watchAuth()
  ]);
}
