import {
  LOGIN_API,
  LOGIN_REQUEST,
  GET,
  POST,
  PUT,
  DEL,
  ALL,
  SELF,
  COMPONENT,
  DEFINITION,
  HISTORY,
  REDIRECT_TO,
  LOGOUT
} from "actions/actionType.js";
import { put, fork, call, take, takeEvery } from "redux-saga/effects";
import graphql from "services/graphql";
import query from "query";
function* callback(isSuccess, error) {
  isSuccess
    ? yield put({
        type: COMPONENT.CALLBACK_SUCCESS
      })
    : yield put({
        type: COMPONENT.CALLBACK_FAIL,
        data: error.response.data.message
      });
  yield put({
    type: COMPONENT.SUCCESS
  });
}
function* loading() {
  return yield put({
    type: COMPONENT.CALLBACK_LOADING
  });
}
function* complete() {
  return yield put({
    type: COMPONENT.SUCCESS
  });
}
function* outOfRange() {
  yield put({
    type: COMPONENT.CALLBACK_OUT
  });
}
function* request(actions) {
  switch (actions.api) {
    case GET:
      return yield fork(get, actions);
    case POST:
      return yield fork(post, actions);
    case PUT:
      return yield fork(change, actions);
    case DEL:
      return yield fork(del, actions);
    case ALL:
      return yield fork(getAll, actions);
    case SEARCH:
      return yield fork(search, actions);
    default:
      null;
  }
}
function* getAll(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* get(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* post(actions) {
  try {
    switch (actions.doc) {
      //////////////////////////////////
      case SELF:
        var response = yield call(
          graphql.restPost,
          "auth/login/chula",
          actions.item.ticket
        );
        yield put({
          type: LOGIN_API.SELF.POST.SUCCESS,
          data: response.data
        });
        if (response.data) {
          return yield put({
            type: REDIRECT_TO,
            redirectTo: "/home"
          });
        } else {
          return yield call(callback, false, "Cannot Login");
        }
      // case LOGOUT:
      //   var response = yield call(graphql.restPost, "auth/logout/chula", null);
      //   return yield put({
      //     type: REDIRECT_TO,
      //     redirectTo: "/login"
      //   });


      //////////////////////////////////

      default:
        null;
    }
  } catch (e) {
    console.log(e);
    yield call(callback, false, e);
  }
}
function* change(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    yield call(callback, false, e);
  }
}
function* del(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* search(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
export function* watchLogin() {
  yield takeEvery(LOGIN_REQUEST, request);
}
