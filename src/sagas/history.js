import {
  HISTORY_API,
  HISTORY_REQUEST,
  GET,
  POST,
  PUT,
  DEL,
  ALL,
  SELF,
  COMPONENT,
  DEFINITION,
  HISTORY,
  STAT
} from "actions/actionType.js";
import { put, fork, call, take, takeEvery } from "redux-saga/effects";
import graphql from "services/graphql";
import query from "query";
function* callback(isSuccess, error) {
  isSuccess
    ? yield put({
        type: COMPONENT.CALLBACK_SUCCESS
      })
    : yield put({
        type: COMPONENT.CALLBACK_FAIL,
        data: error.response.data.message
      });
  yield put({
    type: COMPONENT.SUCCESS
  });
}
function* loading() {
  return yield put({
    type: COMPONENT.CALLBACK_LOADING
  });
}
function* complete() {
  return yield put({
    type: COMPONENT.SUCCESS
  });
}
function* outOfRange() {
  yield put({
    type: COMPONENT.CALLBACK_OUT
  });
}
function* request(actions) {
  switch (actions.api) {
    case GET:
      return yield fork(get, actions);
    case POST:
      return yield fork(post, actions);
    case PUT:
      return yield fork(change, actions);
    case DEL:
      return yield fork(del, actions);
    case ALL:
      return yield fork(getAll, actions);
    case SEARCH:
      return yield fork(search, actions);
    default:
      null;
  }
}
function* getAll(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {

      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* get(actions) {
  try {
    switch (actions.doc) {
      case STAT:
        var response = yield call(graphql.restGet, `/dent/historyStat`,null);
        return yield put({
          type: HISTORY_API.STAT.GET.SUCCESS,
          data: response.data
        });
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* post(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
    yield call(callback, false, e);
  }
}
function* change(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    yield call(callback, false, e);
  }
}
function* del(actions) {
  try {
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
function* search(actions) {
  try {
    yield call(loading);
    switch (actions.doc) {
      default:
        null;
    }
  } catch (e) {
    console.log(e);
  }
}
export function* watchHistory() {
  yield takeEvery(HISTORY_REQUEST, request);
}
