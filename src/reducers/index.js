
import chat from './chat'
import definition from './definition'
import section from './section'
import history from './history'
import {Reducer} from 'deverhood-react'
const {auth,common,user} = Reducer
import { combineReducers } from 'redux';
export default combineReducers({
  section,
  chat,
  definition,
  auth,
  common,
  user,
  history
});
