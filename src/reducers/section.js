import { SECTION_API, SELECT_ANSWER } from "actions/actionType";
const defaultState = {
  list: [],
  data: {}
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SELECT_ANSWER:
      var me = [
        ...state.data.chat,
        {
          ...action.answer,
          isMe: true,
          answers: [],
          question: action.answer.name
        }
      ];
      var list = [
        ...me,
        state.data.questions.filter(v => v.id === action.answer.next).length > 0
          ? ({...state.data.questions.filter(v => v.id === action.answer.next)[0],isMe:false})
          : null
      ];

      return {
        ...state,
        data: {
          ...state.data,
          chat: list
        }
      };
    case SECTION_API.SELF.ALL.SUCCESS:
      return { ...state, list: action.data };
    case SECTION_API.SELF.GET.SUCCESS:
      return {
        ...state,
        data: { ...action.data, chat: [action.data.questions[0]], isMe: false }
      };
    case SECTION_API.DEFINITION.GET.SUCCESS:
      return {
        ...state,
        data: { ...action.data, chat: [{...action.data}], isMe: false }
      };
    default:
      return state;
  }
};
