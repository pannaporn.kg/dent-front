import { HISTORY_API } from "actions/actionType";
const defaultState = {
  list: [],
  data: {}
};

export default (state = defaultState, action) => {
  switch (action.type){
    case HISTORY_API.STAT.GET.SUCCESS:
      return {
        ...state,
        list:[...action.data]
      };
    default:
      return state;
  }
};
