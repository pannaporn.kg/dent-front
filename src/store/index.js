import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = middleware => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export const initStore = initialState => {
  let middleware = [sagaMiddleware];
  if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger());
  }
  const makeStore = createStore(
    rootReducer,
    initialState,
    bindMiddleware(middleware)
  );
  makeStore.sagaTask = sagaMiddleware.run(rootSaga);
  return makeStore;
};
