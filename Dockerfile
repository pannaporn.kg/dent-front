FROM keymetrics/pm2:latest-jessie

COPY src/.next src/.next/
COPY src/routes.js src/
COPY src/server.js src/
COPY next.config.js .
COPY package.json .
COPY pm2.json .
COPY src/public src/public/
COPY env env/

ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install

EXPOSE 3000

CMD [ "pm2-runtime", "start", "pm2.json" ]
