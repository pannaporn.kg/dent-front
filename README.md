# RCostExam
## ระบบข้อสอบสำหรับราชวิทยาลัยแพทย์ออร์โธปิดิกส์แห่งประเทศไทย

## Usage
---
Start dev server
```
npm run dev 
```

Build scripts
```
npm run build 
```
Run in production mode
```
npm start
```

## Features
---
#### Server-side
- Node.JS v6.x.x
- Express
- Next.JS

#### Client-side
- React.js 
- Redux
- Antd


```

## License
---
This project is developed by **Deverhood .**  

